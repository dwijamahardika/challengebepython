from models import User
from sqlalchemy.future import select

from utils import (
    hash_password,
    verify_password,
    ResponseOut, 
    logging_status
)

from datetime import datetime

from .validate_module import validate_unique_username

logger = logging_status()

async def account_number_activation(user_id, pin, db, token):
    """Function to activate account number"""
    async with db as session:
        try:
            if len(pin) != 6:
                logger.warning(f"User_id: {token['user_id']} enter wrong length of pin")
                return ResponseOut("02", f"Length of pin must be 6", [])
            db_user = await session.execute(select(User).filter(User.user_id == user_id))
            user_data = db_user.scalars().first()
            if user_data is None or user_data.deleted_at:
                logger.warning(f"User_id: {token['user_id']} failed to activate their account")
                return ResponseOut("01", f"User with user_id: {user_id} not found", [])
            if user_data.pin == "0":
                user_data.pin = pin
                session.add(user_data)
                await session.commit()
                logger.info(f"User_id: {token['user_id']} success to activate their account")
                return ResponseOut("00", f"Account number activated successfully", [])
            else:
                logger.warning(f"Account number of user with user_id: {user_id} has already been activated")
                return ResponseOut("02", f"Account number has already been activated", [])
        except Exception as e:
            logger.error(f"User_id: {token['user_id']} failed to activate their account. Error message: {str(e)}")
            return ResponseOut("03", f"Account number activated failed", [])

async def get_user_by_id(user_id, pin, db, token):
    """Function to get user data"""
    async with db as session:
        try:
            if len(pin) != 6:
                logger.warning(f"User_id: {token['user_id']} enter wrong length of pin when getting user data")
                return ResponseOut("02", f"Length of pin must be 6", [])
            db_user = await session.execute(select(User).filter(User.user_id == user_id))
            user_data = db_user.scalars().first()
            if user_data is None or user_data.deleted_at:
                logger.warning(f"User with user_id: {user_id} not found, when getting user data")
                return ResponseOut("01", f"User with user_id: {user_id} not found", [])
            elif user_data.pin != pin:
                logger.warning(f"User_id: {token['user_id']} entered wrong pin, when getting user data")
                return ResponseOut("02", "Pin is incorrect", [])
            else:
                logger.info(f"User_id: {token['user_id']} get user data successfully")
                return ResponseOut("00", f"Get data user successfully", [user_data.serialize()])
        except Exception as e:
            logger.error(f"User_id: {token['user_id']} failed to getting user data. Error message: {str(e)}")
            return ResponseOut("03", f"{str(e)}", [])


async def update_user(user_id, old_pin, data, db, token):
    """Function to update user data (can be custom for what data their want to change)"""
    async with db as session:
        try:
            if len(old_pin) != 6:
                logger.warning(f"User_id: {token['user_id']} enter wrong length of pin")
                return ResponseOut("02", f"Length of pin must be 6", [])
            
            db_user = await session.execute(select(User).filter(User.user_id == user_id))
            user_data = db_user.scalars().first()

            if user_data is None or user_data.deleted_at:
                logger.warning(f"User_id: {token['user_id']} access update user. User with user_id: {user_id} not found")
                return ResponseOut("01", f"User with user_id: {user_id} not found", [])

            if user_data.pin != old_pin:
                logger.warning(f"User_id: {token['user_id']} entered wrong pin, when updating user data with user_id: {user_id}")
                return ResponseOut("02", "Pin is incorrect", [])
            
            if not (len(data.new_pin) == 6 or len(data.new_pin) == 0):
                logger.warning(f"User_id: {token['user_id']} entered wrong length of new pin, when updating user data with user_id: {user_id}")
                return ResponseOut("02", "You entered wrong length of new pin", [])
            
            if user_data.pin == "0":
                logger.warning(f"Account has not validated (does not have a pin number) for user_id: {user_data.user_id} when update data")
                return ResponseOut("02", "Account has not validated (does not have a pin number)", [])
            
            if old_pin == data.new_pin:
                logger.warning(f"User_id: {token['user_id']} entered the same old and new pin, when updating user data with user_id: {user_id}")
                return ResponseOut("02", f"Your old pin and new pin are same, please give different new pin to change your current pin", [])
            
            if user_data.username == data.username:
                logger.warning(f"User_id: {token['user_id']} entered the same old and new username, when updating user data with user_id: {user_id}")
                return ResponseOut("02", f"Your old username and new username are same, please give different new username to change your current username", [])
            
            if user_data.name == data.name:
                logger.warning(f"User_id: {token['user_id']} entered the same old and new name, when updating user data with user_id: {user_id}")
                return ResponseOut("02", f"Your old name and new name are same, please give different name to change your current name", [])
            
            if user_data.phone_number == data.phone_number:
                logger.warning(f"User_id: {token['user_id']} entered the same old and new phone number, when updating user data with user_id: {user_id}")
                return ResponseOut("02", f"Your old phone number and new phone number, please give different phone number to change your current phone number", [])
            
            if await verify_password(data.password, user_data.password):
                logger.warning(f"User_id: {token['user_id']} entered the same old and new password, when updating user data with user_id: {user_id}")
                return ResponseOut("02", f"Your old password and new password are same, please give different new password to change your current password", [])

            # update user data
            if data.username:
                if await validate_unique_username(data.username, db):
                    user_data.username = data.username
                else:
                    logger.warning(f"User_id: {token['user_id']} entered non-unique username, when updating user data with user_id: {user_id}")
                    return ResponseOut("02", f"Username not unique", [])
                
            if data.name:
                user_data.name = data.name
            if data.phone_number:
                user_data.phone_number = data.phone_number
            if data.password:
                hashed_password = await hash_password(data.password)
                user_data.password = hashed_password.decode()
            if old_pin and data.new_pin:
                user_data.pin = data.new_pin

            if data.username or data.name or data.phone_number or data.password or (old_pin and data.new_pin):
                user_data.updated_at = datetime.now()
                session.add(user_data)
                await session.commit()
                logger.info(f"User_id: {token['user_id']} updated user data with user_id: {user_id} successfully")
                return ResponseOut("00", "User data updated successfully", [{"user_id": user_id, "updated_at": str(user_data.updated_at)}])
            else:
                logger.info(f"User_id: {token['user_id']} updated empty user data with user_id: {user_id}")
                return ResponseOut("02", "No user data updated", [{"user_id": user_id}])
        except Exception as e:
            logger.error(f"User_id: {token['user_id']} failed to update user data. Error message: {str(e)}")
            return ResponseOut("03", f"{str(e)}", [])

async def delete_user(user_id, pin, db, token):
    """Function to delete user account (soft delete)"""
    async with db as session:
        try:
            if len(pin) != 6:
                logger.warning(f"User_id: {token['user_id']} enter wrong length of pin deleting user account")
                return ResponseOut("02", f"Length of pin must be 6", [])
            
            db_user = await session.execute(select(User).filter(User.user_id == user_id))
            user_data = db_user.scalars().first()

            if user_data.pin != pin:
                logger.warning(f"User_id: {token['user_id']} entered wrong pin, when deleting account with user_id: {user_id}")
                return ResponseOut("02", "Pin is incorrect", [])
            
            elif user_data is None or user_data.deleted_at:
                logger.warning(f"User_id: {token['user_id']} access delete account. User with user_id: {user_id} not found")
                return ResponseOut("01", f"User with user_id: {user_id} not found", [])
            
            if user_data.pin == "0":
                logger.warning(f"Account has not validated (does not have a pin number) for user_id: {user_data.user_id} when deleting user account")
                return ResponseOut("02", "Account has not validated (does not have a pin number)", [])

            user_data.deleted_at = datetime.now()
            session.add(user_data)
            await session.commit()

            logger.info(f"User_id: {token['user_id']} deleted user with user_id: {user_id}")
            return ResponseOut("00", f"User with user_id: {user_id} has been deleted", [])
        except Exception as e:
            logger.error(f"User_id: {token['user_id']} failed to delete user. Error message: {str(e)}")
            return ResponseOut("03", f"{str(e)}", [])