import random
from datetime import datetime, timedelta

from sqlalchemy.future import select

from models import User
from utils import (
    ResponseOut,
    create_access_token,
    hash_password,
    verify_password,
    logging_status
)
from .validate_module import validate_unique_username

ACCESS_TOKEN_EXPIRE_MINUTES = 30
logger = logging_status()

async def register_user(user, db):
    async with db as session:
        try:
            name, username, password, phone_number = (
                user.name,
                user.username,
                user.password,
                user.phone_number,
            )

            account_number = str(random.randint(1000000000, 9999999999))

            hashed_password = await hash_password(password)
            is_unique_username = await validate_unique_username(username, db)

            if is_unique_username:
                new_user = User(
                    name=name,
                    username=username,
                    password=hashed_password.decode(),
                    phone_number=phone_number,
                    account_number=account_number,
                )
                session.add(new_user)
                await session.commit()

                response_data = [{"name": name, "username": username}]
                logger.info(f"User with username: {username}  registered successfully")
                return ResponseOut("00", "User registered successfully", response_data)
            else:
                logger.warning(f"User registration failed: {username} has been registered.")
                return ResponseOut(
                    "02", f"User registration failed: {username} has been registered.", []
                )
        except Exception as e:
            logger.error(f"User registration failed: {str(e)}")
            return ResponseOut("03", f"User registration failed: {str(e)}", [])


async def login_user(user, db):
    async with db as session:
        try:
            db_user = await session.execute(select(User).filter(User.username == user.username))
            result = db_user.scalars().first()
            if result is None or result.deleted_at:
                logger.warning(f"User with username: {user.username} not found")
                return ResponseOut("02", f"User with username: {user.username} not found", [])
            
            if result and await verify_password(user.password, result.password):
                access_date = str(datetime.now())
                token_data = {"user_id": result.user_id, "access_date": access_date}
                token = create_access_token(
                    data=token_data,
                    expires_delta=timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES),
                )

                response_data = [{"token_bearer": token}]
                logger.info(f"User with user_id: {result.user_id} Success to Login")
                return ResponseOut("00", "Login Success", response_data)
            else:
                logger.warning(f"User with username: {user.username} Failed to Login")
                return ResponseOut(
                    "02", "Login Failed, please check your email or password", []
                )
        except Exception as e:
            logger.error(f"Login Failed: {str(e)}")
            return ResponseOut("03", f"{str(e)}", [])