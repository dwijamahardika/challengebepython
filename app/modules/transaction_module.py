from models import User, Transaction
from sqlalchemy.future import select
from sqlalchemy import and_
from utils import ResponseOut, logging_status
from datetime import datetime

logger = logging_status()

async def topup_user(user, db):
    """Function to retrieve balance from top up"""
    async with db as session:
        try:
            
            user_id = user.user_id
            pin = user.pin
            amount = user.amount
            account_number = user.account_number

            if len(pin) != 6:
                logger.warning(f"User_id: {user_id} enter wrong length of pin when top up")
                return ResponseOut("02", f"Length of pin must be 6", [])

            # validate user
            db_user = await session.execute(select(User).filter(User.user_id == user_id))
            user_data = db_user.scalars().first()

            if user_data is None or user_data.deleted_at:
                logger.warning(f"User with user_id: {user_data.user_id} not found when topup balance")
                return ResponseOut("01", f"User with user_id: {user_id} not found", [])
            
            #validate pin
            if user_data.pin != pin:
                logger.warning(f"User with user_id: {user_data.user_id} entered incorrect pin when topup balance")
                return ResponseOut("02", "Pin is incorrect", [])

            # validate account number
            if len(user_data.account_number) != 10 or account_number != user_data.account_number:
                logger.warning(f"Invalid account number: {user_data.account_number} when topup balance")
                return ResponseOut("02", f"Invalid account number", [])

            # validate amount
            if amount <= 0:
                logger.warning(f"Invalid amount for user_id: {user_data.user_id}, must be greater than 0 when topup balance")
                return ResponseOut("02", "Invalid amount, must be greater than 0", [])
            if amount > 100000000:
                logger.warning(f"Amount limit exceeded for user_id: {user_data.user_id}, must be less than or equal to 1,000,000,000 when topup balance")
                return ResponseOut("02", "Amount limit exceeded, must be less than or equal to 1,000,000,000", [])
            
            if user_data.pin == "0":
                logger.warning(f"Account has not validated (does not have a pin number) for user_id: {user_data.user_id} when topup balance")
                return ResponseOut("02", "Account has not validated (does not have a pin number)", [])

            # update user balance
            new_balance = user_data.balance + amount
            user_data.balance = new_balance
            user_data.updated_at = datetime.now()
            session.add(user_data)

            # save transaction
            transaction = Transaction(
                user_id=user_id,
                transaction_type='topup',
                amount=amount,
                destination_account_number=user_data.account_number,
            )
            session.add(transaction)

            await session.commit()

            response_data = [{"user_id": user_id, "new_balance": str(new_balance)}]
            logger.info(f"User with user_id: {user_data.user_id} success to topup balance", response_data=response_data)
            return ResponseOut("00", "Topup successful", response_data)
        
        except Exception as e:
            logger.error(f"Topup Failed: {str(e)}")
            return ResponseOut("03",f"{str(e)}", [])

async def transfer_user(data, db):
    """Function to transfer from user to another user"""
    async with db as session:
        try:
            if len(data.pin) != 6:
                logger.warning(f"User_id: {data.user_id} enter wrong length of pin when transfer")
                return ResponseOut("02", f"Length of pin must be 6", [])
            
            db_user = await session.execute(select(User).filter(User.user_id == data.user_id))
            user_data = db_user.scalars().first()
            if user_data is None or user_data.deleted_at:
                logger.warning(f"User with user_id: {user_data.user_id} not found when transfer")
                return ResponseOut("01", f"User with user_id: {data.user_id} not found", [])
            
            if len(user_data.account_number) != 10 or user_data.account_number != data.from_account_number:
                logger.warning(f"User with user_id: {user_data.user_id} entered invalid account number when transfer")
                return ResponseOut("02", f"Invalid account number", [])
            
            if user_data.account_number == data.destination_account_number:
                logger.warning(f"User with user_id: {user_data.user_id} trying transfer to own account number")
                return ResponseOut("02", f"Cannot transfer to own account number", [])
            
            if user_data.pin != data.pin:
                logger.warning(f"User with user_id: {user_data.user_id} entered incorrect pin when transfer")
                return ResponseOut("02", "Pin is incorrect", [])
            
            if user_data.pin == "0":
                logger.warning(f"Account has not validated (does not have a pin number) for user_id: {user_data.user_id} when transfer")
                return ResponseOut("02", "Account has not validated (does not have a pin number)", [])
            
            if len(data.destination_account_number) != 10:
                logger.warning(f"Invalid destination account number: {data.destination_account_number}, source account number: {user_data.account_number}")
                return ResponseOut("02", f"Invalid destination account number", [])

            # validate amount
            if data.amount <= 0:
                logger.warning(f"Invalid amount for user_id: {user_data.user_id}, must be greater than 0 when transfer")
                return ResponseOut("02", "Invalid amount, must be greater than 0", [])
            elif data.amount > 100000000:
                logger.warning(f"Amount limit exceeded for user_id: {user_data.user_id}, must be less than or equal to 1,000,000,000 when transfer")
                return ResponseOut("02", "Amount limit exceeded, must be less than or equal to 100,000,000", [])
            
            # check if user has enough balance
            if user_data.balance < data.amount:
                logger.warning(f"Insufficient balance for user_id: {user_data.user_id}")
                return ResponseOut("02", "Insufficient balance", [])

            # check if user has reached transfer limit
            daily_transactions = await session.execute(
                select(Transaction).filter(
                    and_(
                        Transaction.user_id == user_data.user_id,
                        Transaction.transaction_type == 'transfer out',
                        Transaction.created_at >= datetime.now().replace(hour=0, minute=0, second=0, microsecond=0),
                        Transaction.created_at <= datetime.now().replace(hour=23, minute=59, second=59, microsecond=999999)
                    )
                )
            )
            total_daily_transactions = sum([t.amount for t in daily_transactions.scalars().all()])
            if (total_daily_transactions + data.amount) > 100000000:
                logger.warning(f"Daily transfer limit reached for user_id: {user_data.user_id}")
                return ResponseOut("02", "Daily transfer limit reached", [])

            # check if destination account exists
            db_destination = await session.execute(select(User).filter(User.account_number == data.destination_account_number))
            destination = db_destination.scalars().first()
            if destination is None or destination.deleted_at:
                logger.warning(f"Destination account not found, account number: {data.destination_account_number}, when user_id: {user_data.user_id} transferred")
                return ResponseOut("02", f"Destination account not found", [])
            
            if destination.pin == "0":
                logger.warning(f"Account Destination has not validated (does not have a pin number) for user_id: {user_data.user_id} when topup balance")
                return ResponseOut("02", "Account Destination has not validated (does not have a pin number)", [])

            # update balance for both user and destination
            destination.balance += data.amount
            session.add(destination)

            user_data.balance -= data.amount
            session.add(user_data)
            
            # save transaction for both user and destination
            transaction = Transaction(
                user_id=data.user_id,
                transaction_type='transfer out',
                amount=data.amount,
                destination_account_number=data.destination_account_number,
            )
            session.add(transaction)

            transaction = Transaction(
                user_id=destination.user_id,
                transaction_type='transfer in',
                amount=data.amount,
                destination_account_number=data.destination_account_number,
            )
            session.add(transaction)

            await session.commit()
            response_data = [{
                "Source Account Number": user_data.account_number, 
                "Destination Account Number": data.destination_account_number, 
                "Amount": data.amount
                }]
            logger.info(f"User with user_id: {user_data.user_id} success to topup balance", response_data=response_data)
            return ResponseOut("00", "Transfer success", response_data)

        except Exception as e:
            logger.error(f"Transaction Failed: {str(e)}")
            return ResponseOut("03",f"{str(e)}", [])

async def history_user(data, pin, db, token):
    """Function to get get transaction history based on page number and pagination"""
    async with db as session:
        try:

            if len(pin) != 6:
                logger.warning(f"User_id: {token['user_id']} enter wrong length of pin when get transaction history")
                return ResponseOut("02", f"Length of pin must be 6", [])
            
            db_user = await session.execute(select(User).filter(User.user_id == token['user_id']))
            user_data = db_user.scalars().first()
            if pin != user_data.pin:
                logger.warning(f"User_id: {token['user_id']} entered incorrect pin when get transaction history")
                return ResponseOut("02", f"Pin is incorrect", [])
            
            if data.pagination in [5, 10, 20]:
                offset = (data.page - 1) * data.pagination
                transactions = await session.execute(select(Transaction).filter(Transaction.user_id == token['user_id']).offset(offset).limit(data.pagination))
                list_transactions = [transaction.serialize() for transaction in transactions.scalars().all()]
                if not transactions:
                    logger.warning(f"Transaction not found with user_id: {token['user_id']} when access transaction history")
                    return ResponseOut("01", "Transactions Not Found", [])
                elif list_transactions == []:
                    logger.warning(f"Transaction not found with user_id: {token['user_id']} when access transaction history")
                    return ResponseOut("02", "Transactions for this page and pagination not found", list_transactions)
                else:
                    logger.info(f"Get history transactions success for user_id: {token['user_id']}")
                    return ResponseOut("00", "Get History Successfully", list_transactions)
            else:
                logger.warning(f"User_id: {token['user_id']} entered wrong number of paginations")
                return ResponseOut("02", f"User_id: {token['user_id']} entered wrong number of paginations, paginations only 5/10/20", [])
        
        except Exception as e:
            logger.error(f"Get history transactions Failed: {str(e)}")
            return ResponseOut("03", f"{str(e)}", [])