from models import User
from sqlalchemy.future import select
from sqlalchemy import and_

async def validate_unique_username(username, db):
    """Function to validate a unique username"""
    async with db as session:
        db_users = await session.execute(select(User).filter(User.username == username))
        users = db_users.scalars().first()
        if users is None:
            return True
        else:
            return False