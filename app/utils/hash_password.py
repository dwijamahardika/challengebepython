import bcrypt

async def hash_password(data):
    """Function to hash password"""
    return bcrypt.hashpw(data.encode('utf-8'), bcrypt.gensalt())

async def verify_password(data, hashed_password):
    """Function to verify password"""
    return bcrypt.checkpw(data.encode('utf-8'), hashed_password.encode('utf-8'))