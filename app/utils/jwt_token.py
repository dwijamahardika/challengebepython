import jwt

from fastapi import Depends, HTTPException
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from jwt import PyJWTError, decode
from datetime import datetime, timedelta
from typing import Optional

from .response import ResponseOut

SECRET_KEY = "secret_key"
ALGORITHM = "HS256"
BLACKLIST = set()

bearer_scheme = HTTPBearer()

def create_access_token(data: dict, expires_delta: Optional[timedelta] = None):
    """Function to create access token with given data and expiration time options"""
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


async def verify_token(credentials: HTTPAuthorizationCredentials = Depends(bearer_scheme)):
    """Function to verify tokens"""
    try:
        token = credentials.credentials
        payload = decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        now = datetime.utcnow()
        expire = datetime.fromtimestamp(payload["exp"])
        if now > expire:
            raise HTTPException(status_code=401, detail="Token has expired")
        if token in BLACKLIST:
            raise HTTPException(status_code=401, detail="User has logout")
    except PyJWTError:
        raise HTTPException(status_code=401, detail="Invalid Token")
    return payload


async def refresh_token(token):
    """Function to refresh token with given data"""
    dt = {
        "user_id": token['user_id'],
        "access_date": str(datetime.now())
    }
    _token = create_access_token(data=dt, expires_delta=timedelta(minutes=30))
    return ResponseOut(message="00", status="Success Refresh Token", data= {"token_bearer": _token})


async def is_token_blacklisted(credentials: HTTPAuthorizationCredentials = Depends(bearer_scheme)):
    """Function to turn off the token"""
    token = credentials.credentials
    if token in BLACKLIST:
        raise HTTPException(status_code=401, detail="has expired")
    BLACKLIST.add(token)
    return ResponseOut('00', f'Logout Successfully', [])