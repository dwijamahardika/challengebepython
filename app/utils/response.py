def ResponseOut(message, status, data):
    """Function to format response"""
    out_rsp = {
        "message_id" : message,
        "status" : status,
        "data" : data
    }
    return out_rsp