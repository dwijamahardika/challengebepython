"""Connetion to PostgreSQL"""

import os
from sqlalchemy import MetaData
from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from dotenv import load_dotenv

load_dotenv()

POSTGRES_DB_API_USER: str = os.getenv('POSTGRES_DB_API_USER')
POSTGRES_DB_API_PASSWORD: str = os.getenv('POSTGRES_DB_API_PASSWORD')
POSTGRES_DB_HOST: str = os.getenv('POSTGRES_DB_HOST')
POSTGRES_DB_EXPOSE_PORT: str = os.getenv('POSTGRES_DB_EXPOSE_PORT')
POSTGRES_DB_API: str = os.getenv('POSTGRES_DB_API')

SQLALCHEMY_DATABASE_URL = f"postgresql+asyncpg://{POSTGRES_DB_API_USER}:{POSTGRES_DB_API_PASSWORD}@{POSTGRES_DB_HOST}:{POSTGRES_DB_EXPOSE_PORT}/{POSTGRES_DB_API}"
print(f'from connection core : {SQLALCHEMY_DATABASE_URL}')
engine = create_async_engine(SQLALCHEMY_DATABASE_URL, echo=True)

metadata = MetaData()
Base = declarative_base(metadata=metadata)

async_session = sessionmaker(bind=engine, class_=AsyncSession, expire_on_commit=False)

async def get_async_session():
    db = async_session()
    try:
        yield db
    finally:
        await db.close()
