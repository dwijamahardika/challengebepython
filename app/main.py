import uvicorn
from fastapi import FastAPI
from utils import engine
from models import Base
from api import api_router

app = FastAPI(
    title="Challenge BE",
    docs_url = "/api/docs"
)

app.include_router(api_router, prefix="/api/v1")

@app.on_event("startup")
async def startup():
    async with engine.begin() as conn:
        # await conn.run_sync(Base.metadata.drop_all)
        await conn.run_sync(Base.metadata.create_all)

if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8001)