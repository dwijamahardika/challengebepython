from modules import register_user, login_user
from fastapi import Depends, APIRouter
from schema import RegisterUser, LoginUser
from sqlalchemy.ext.asyncio import AsyncSession
from utils import get_async_session, is_token_blacklisted

router = APIRouter()

@router.post("/register",)
async def register(user: RegisterUser, db: AsyncSession = Depends(get_async_session)):
    return await register_user(user, db)

@router.post("/login",)
async def login(user: LoginUser, db: AsyncSession = Depends(get_async_session)):
    return await login_user(user, db)

@router.post("/logout",)
async def logout(token : str = Depends(is_token_blacklisted)):
    return token