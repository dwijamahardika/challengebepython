from fastapi import APIRouter

# from .user_api import *
from .auth_api import *
from .crud_api import *
from .token_api import *
from .transaction_api import *

api_router = APIRouter()

api_router.include_router(token_api.router, tags=['Token API'])
api_router.include_router(auth_api.router, tags=['AUTH API'])
api_router.include_router(crud_api.router, tags=['CRUD API'])
api_router.include_router(transaction_api.router, tags=['Transaction API'])