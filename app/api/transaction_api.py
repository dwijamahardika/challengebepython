from fastapi import Depends, APIRouter

from modules import (
    topup_user, 
    transfer_user, 
    history_user
)

from schema import (
    TopUpUser, 
    TransferUser, 
    HistoryUser
)

from sqlalchemy.ext.asyncio import AsyncSession
from utils import get_async_session, verify_token

router = APIRouter()

@router.post("/topup",)
async def topup(user: TopUpUser, db: AsyncSession = Depends(get_async_session), token : dict = Depends(verify_token)):
    return await topup_user(user, db)

@router.post("/transfer",)
async def transfer(user: TransferUser, db: AsyncSession = Depends(get_async_session), token : dict = Depends(verify_token)):
    return await transfer_user(user, db)

@router.post("/history",)
async def history(user: HistoryUser, pin: str, db: AsyncSession = Depends(get_async_session), token : dict = Depends(verify_token)):
    return await history_user(user, pin, db, token)