from fastapi import Depends, APIRouter
from utils import verify_token, refresh_token

router = APIRouter()

@router.post("/refresh_token")
async def refresh(_token : dict = Depends(verify_token)):
    return await refresh_token(_token)