from fastapi import Depends, APIRouter
from schema import UpdateUser

from modules import (
    account_number_activation, 
    get_user_by_id, 
    update_user, 
    delete_user
)

from sqlalchemy.ext.asyncio import AsyncSession
from utils import get_async_session, verify_token

router = APIRouter()

@router.post("/activation/{id}")
async def account_activation(id: int, new_pin: str, db: AsyncSession = Depends(get_async_session), token : dict = Depends(verify_token)):
    return await account_number_activation(id, new_pin, db, token)

@router.get("/account/{id}",)
async def get_account(id: int, pin: str, db: AsyncSession = Depends(get_async_session), token : dict = Depends(verify_token)):
    return await get_user_by_id(id, pin, db, token)

@router.put("/account/{id}",)
async def update(id: int, pin: str ,data: UpdateUser, db: AsyncSession = Depends(get_async_session), token : dict = Depends(verify_token)):
    return await update_user(id, pin, data, db, token)

@router.delete("/account/{id}",)
async def delete(id: int, pin: str, db: AsyncSession = Depends(get_async_session), token : dict = Depends(verify_token)):
    return await delete_user(id, pin, db, token)