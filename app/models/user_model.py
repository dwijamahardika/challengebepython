from sqlalchemy import(
    Column, String, 
    BigInteger, DateTime, 
    Numeric
)

from datetime import datetime
from utils import Base

class User(Base):
    __tablename__ = 'users'
    user_id = Column(BigInteger, primary_key=True)
    name = Column(String(50), nullable=False)
    username = Column(String(50), nullable=False)
    password = Column(String(100), nullable=False)
    phone_number = Column(String(15), nullable=False)
    account_number = Column(String(10), nullable=False)
    pin = Column(String(6), default="0")
    balance = Column(Numeric(12,2), default=0)
    created_at = Column(DateTime, default=datetime.now())
    updated_at = Column(DateTime)
    deleted_at = Column(DateTime)

    def __init__(self, name, username, password, phone_number, account_number):
        self.name = name
        self.username = username
        self.password = password
        self.phone_number = phone_number
        self.account_number = account_number

    def serialize(self):
        return {
            'user_id': self.user_id,
            'name': self.name,
            'phone_number': self.phone_number,
            'account_number': self.account_number,
            'balance': str(self.balance)
        }
