from sqlalchemy import (
    Column, String, 
    BigInteger, DateTime, 
    Numeric, ForeignKey
)

from sqlalchemy.orm import relationship
from datetime import datetime
from .user_model import User
from utils import Base

class Transaction(Base):
    __tablename__ = 'transactions'
    transaction_id = Column(BigInteger, primary_key=True)
    user_id = Column(BigInteger, ForeignKey('users.user_id'))
    transaction_type = Column(String(20), nullable=False)
    transaction_time = Column(DateTime, default=datetime.now())
    destination_account_number = Column(String(10))
    amount = Column(Numeric(12,2), nullable=False)
    created_at = Column(DateTime, default=datetime.now())
    updated_at = Column(DateTime)
    deleted_at = Column(DateTime)

    user = relationship(User, backref='transactions')

    def __init__(self, user_id, transaction_type, amount, destination_account_number):
        self.user_id = user_id
        self.transaction_type = transaction_type
        self.amount = amount
        self.destination_account_number = destination_account_number

    def serialize(self):
        return {
            'transaction_time': self.transaction_time.strftime("%Y-%m-%d %H:%M:%S"),
            'transaction_type': self.transaction_type,
            'amount': self.amount,
            'destination_account_number': self.destination_account_number,
        }