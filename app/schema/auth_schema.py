from pydantic import BaseModel

class RegisterUser(BaseModel):
    name: str
    username: str
    password: str
    phone_number: str

class LoginUser(BaseModel):
    username: str
    password: str