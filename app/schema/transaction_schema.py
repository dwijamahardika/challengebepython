from pydantic import BaseModel
from decimal import Decimal

class TopUpUser(BaseModel):
    user_id: int
    account_number: str
    pin: str
    amount: Decimal

class TransferUser(BaseModel):
    user_id: int
    from_account_number: str
    destination_account_number: str
    pin: str
    amount: Decimal

class HistoryUser(BaseModel):
    page: int
    pagination: int