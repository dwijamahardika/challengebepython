from pydantic import BaseModel
from typing import Optional

class UpdateUser(BaseModel):
    username: Optional[str] = ""
    name: Optional[str] = ""
    phone_number: Optional[str] = ""
    password: Optional[str] = ""
    new_pin: Optional[str] = ""

class UpdatePinUser(BaseModel):
    old_pin: str = "0"
    new_pin: str

class DeleteUser(BaseModel):
    pin: int