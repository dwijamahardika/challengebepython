# Challenge Backend Python

This Project was initialized to provide Rest API for E-Money Application in Python using FastAPI framework.

## Project Structure
- [ ] `app/`
  - [ ] `api/`: serve routing endpoint.
  - [ ] `models/`: contains initialize table.
  - [ ] `modules/`: contains logic commands and queries.
  - [ ] `schema/`: contains schema for request body.
  - [ ] `utils/`: contains utility files.

## Custom Response Status
```bash
00: Success
01: Data not found
02: Process not success
03: Exception raise
```

## Prerequisites

1. Build docker container for PostgresSQL

```bash
docker compose -d db
```

2. Install requirements.txt

```bash
pip install -r requirements.txt
```

3. Duplicate sample.env, then rename it to .env

4. Set up your database based on .env

## Quick Start
1. Start the server:
```bash
cd app
uvicorn main:app --reload
```
2. Open Swagger for API documentation:
In your browser, open 
```css
http://127.0.0.1:8000/api/docs
```

## Built With

* [FastAPI] - The rest framework used
* [Pip] - Dependency Management
* [Docker] - Container Management

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.